# STEPS

1. Open the psql shell

2. \l -> get list of DBs

3. CREATE DATABASE vision;

4. \l -> get list of DBs ... vision should be in the list.

5. Update -> jdbc:postgresql://localhost:5433/vision -> application.properties

6. Run the application -> mvn spring-boot:run

7. \c vision -> Connect to database vision 

8. Run application in main class 
   # JPA will create tables and relations in db

9. \d -> Describes list of relations eg tables
   \dt

10. \d app_table -> Describes the specific table.
    \d app_table_2
    \d user_table_3
    
11. SELECT * FROM app_table;

12. Execute a request on Postman -> 
    http://localhost:8080/api/v1/registration