package com.example.postgresqljparestapis.controllers;

import com.example.postgresqljparestapis.models.Constellation;
import com.example.postgresqljparestapis.services.ConstellationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class ConstellationController {

    @Autowired
    private ConstellationService constellationService;

    // Get All Constellations
    @GetMapping("/constellations")
    private List<Constellation> getAllConstellations() {
        return constellationService.getConstellations();
    }

    // Get One Constellation By ID
    @GetMapping("/constellation/{id}")
    private Constellation getConstellation(@PathVariable Long id) {
        return constellationService.getConstellationById(id);
    }

    // Get One Constellation By Name
    @GetMapping("/constellationByName/{name}")
    private Constellation getConstellationByName(@PathVariable String name) {
        return constellationService.getConstellationByName(name);
    }

    // Post One Constellation
    @PostMapping("/constellation")
    private Constellation createConstellation(@RequestBody Constellation constellation) {
        return constellationService.saveConstellation(constellation);
    }

    // Post Multiple Constellations
    @PostMapping("/constellations")
    private List<Constellation> createConstellations(@RequestBody List<Constellation> constellations) {
        return constellationService.saveConstellations(constellations);
    }

    // Delete A Constellation By ID
    @DeleteMapping("/constellation/{id}")
    private String deleteConstellation(@PathVariable Long id) {
        return constellationService.deleteConstellation(id);
    }

    // Update A Constellation
    @PutMapping("/constellation")
    private Constellation updateConstellation(@RequestBody Constellation constellation) {
        return constellationService.updateConstellation(constellation);
    }
}
