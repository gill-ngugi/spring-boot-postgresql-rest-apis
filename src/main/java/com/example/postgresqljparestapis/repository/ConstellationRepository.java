package com.example.postgresqljparestapis.repository;

import com.example.postgresqljparestapis.models.Constellation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ConstellationRepository extends JpaRepository<Constellation, Long> {
    Optional<Constellation> findByName(String name);
}
