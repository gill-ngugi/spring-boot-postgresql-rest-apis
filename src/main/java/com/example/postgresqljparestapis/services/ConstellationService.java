package com.example.postgresqljparestapis.services;

import com.example.postgresqljparestapis.models.Constellation;
import com.example.postgresqljparestapis.repository.ConstellationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConstellationService {

    @Autowired
    private ConstellationRepository constellationRepository;

    // Get All Constellations
    public List<Constellation> getConstellations() {
        return constellationRepository.findAll();
    }

    // Get One Constellation By ID
    public Constellation getConstellationById(Long id) {
        return constellationRepository.findById(id).orElse(null);
    }

    // Get One Constellation By Name
    public Constellation getConstellationByName(String name) {
        return constellationRepository.findByName(name).orElse(null);
    }

    // Post One Constellation
    public Constellation saveConstellation(Constellation constellation) {
        return constellationRepository.save(constellation);
    }

    // Post Multiple Constellations
    public List<Constellation> saveConstellations(List<Constellation> constellations) {
        return constellationRepository.saveAll(constellations);
    }

    // Delete A Constellation By ID
    public String deleteConstellation(Long id) {
        constellationRepository.deleteById(id);
        return "Constellation with ID " + id + " deleted!";
    }

    // Update A Constellation
    public Constellation updateConstellation(Constellation constellation) {
        Constellation existingConstellation = constellationRepository.findById(constellation.getId()).orElse(null);
        existingConstellation.setName(constellation.getName());
        existingConstellation.setStars(constellation.getStars());
        return constellationRepository.save(existingConstellation);
    }
}
